<?php
/**
 * SCSSPHP
 *
 * @copyright 2012-2020 Leaf Corcoran
 *
 * @license http://opensource.org/licenses/MIT MIT
 *
 * @link http://leafo.github.io/scssphp
 */

//namespace Leafo\ScssPhp;
use ScssPhp\ScssPhp\Compiler;


try {
    $scss = new Compiler();
    
    $scss->setFormatter('ScssPhp\ScssPhp\Formatter\Compressed');
    $scss->setImportPaths($layoutpath.'/scss');


        //$bs4_imports = '@import "'.$layoutpath.'/scss/base.scss";';
        $bs4_imports = '@import "base.scss";';
        $buf_debug += addDebug('BASE SASS | ', 'cubes', $layoutpath .'/scss/base.scss', $startmicro, 'table-default', 'loadphpcss_base.php');
       
        //$scss->setFormatter('scss_formatter_compressed');
        $cssOut = $scss->compile($bs4_imports);

        //Check cache directory is created
        if (!file_exists($cachepath)) {
            mkdir($cachepath, 0777, true);
        }
        file_put_contents($cachepath . '/base.css', $cssOut);

        //$cosa = $cssOut;


} catch (\Exception $e) {
    echo '';
    syslog(LOG_ERR, 'scssphp: Unable to compile content');
    var_dump('scssphp: Unable to compile content');
}

