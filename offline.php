<?php
/**
 * @package     Joomla.Site
 * @subpackage  Template.system
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/** @var JDocumentHtml $this */

$app = JFactory::getApplication();

// Output as HTML5
$this->setHtml5(true);

// Add html5 shiv
JHtml::_('script', 'jui/html5.js', array('version' => 'auto', 'relative' => true, 'conditional' => 'lt IE 9'));

// Styles
JHtml::_('stylesheet', 'templates/system/css/offline.css', array('version' => 'auto'));

if ($this->direction === 'rtl')
{
  JHtml::_('stylesheet', 'templates/system/css/offline_rtl.css', array('version' => 'auto'));
}

JHtml::_('stylesheet', 'templates/system/css/general.css', array('version' => 'auto'));

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

$twofactormethods = JAuthenticationHelper::getTwoFactorMethods();
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <jdoc:include type="head" />

      <style>
      img.img_pral{
        width: 100%;
        height: auto;
      }

    </style>
</head>
<body>
  <jdoc:include type="message" />


    <img class="img_pral" src="Update-sotomanrique.png"/>



  <div class="social hidden-xs">

    <a class="facebook" href="https://www.facebook.com/sotoymanriquevo/#" target="_blank">  
      <img src="http://sotoymanriquevo.com/images/graficos/social/icon-fb.png" alt="soto manrique">
    </a>
    <a class="twitter" href="https://twitter.com/sotoymanriquevo" target="_blank">  
      <img src="/images/graficos/social/icon-twitter.png" alt="soto manrique">
    </a>
    <a class="youtube" href="https://www.youtube.com/channel/UCQ6T8JxoJukKpDs-NquX2hQ?spfreload=5" target="_blank"> 
      <img src="/images/graficos/social/icon-youtube.png" alt="soto manrique">
    </a>

    <a class="instagram" href="https://www.instagram.com/sotoymanriquevo/?hl=es" target="_blank"> 
      <img src="/images/graficos/social/icon-instagram.png" alt="soto manrique">
    </a>

    <a class="mail" href="mailto:info@sotoymanriquevo.com" target="_blank"> 
      <img src="/images/graficos/social/icon-mail.png" alt="soto manrique">
    </a>

  </div>





  </div>
</body>
</html>
