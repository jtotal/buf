<?php defined( '_JEXEC' ) or die; 

jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

$buf_path = JPATH_SITE.'/templates/buf';
$vendorpath = JPATH_SITE.'/templates/buf/vendor';
$libspath = JPATH_SITE.'/templates/buf/libs';

//versions file

//if not exists version.json 
if (!JFile::exists($libspath.'/versions.json')) {
	write_version_json();
}



$c_lock = json_decode(JFile::read($buf_path.'/composer.lock'));
$control_version = json_decode(JFile::read($libspath.'/versions.json'));


//check packages
if( count($c_lock->packages) != count((array) $control_version->libs) ){
	write_version_json(true);
	$control_version = json_decode(JFile::read($libspath.'/versions.json'));
}


foreach ($c_lock->packages as $key => $package) {
	
	$real_name = real_packagename($package->name);

	if($package->version != $control_version->libs->$real_name){
		update_packages($real_name);

		//update version
		$control_version->libs->$real_name = $package->version;
	}
}


//WRITE VERSION FILE UPDATED
JFile::write($libspath.'/versions.json',json_encode($control_version));



/*********TODO****************/
//this is very unelegant


function write_version_json($new=false){

	$buf_path = JPATH_SITE.'/templates/buf';
	$vendorpath = JPATH_SITE.'/templates/buf/vendor';
	$libspath = JPATH_SITE.'/templates/buf/libs';

	if (!JFile::exists($buf_path.'/composer.lock')) {
		echo "ERROR in composer.lock";
	}else{
		$c_lock = json_decode(JFile::read($buf_path.'/composer.lock'));
		
		$versions = array();
		foreach ($c_lock->packages as $key => $package) {
				$versions[real_packagename($package->name)] = $package->version;
			if($new){
				//must reload all
				$versions[real_packagename($package->name)] = '0.0.0';
			}
			
		}

		$control_version = json_encode(array('libs'=>$versions));

		JFile::write($libspath.'/versions.json',$control_version);
	}
} 





function check_versions($c_version='',$buf_version=''){

	if($c_version == $buf_version){
		return true;
	}else{
		return false;
	}
}


function real_packagename($package){

	$real = explode('/', $package);
	return end($real);
}


function update_packages($package){

	$libspath = JPATH_SITE.'/templates/buf/libs';
	$vendorpath = JPATH_SITE.'/templates/buf/vendor';

	switch ($package) {

	    case 'font-awesome':


	    	if(!JFolder::exists($libspath.'/'.$package)){
	    		JFolder::create($libspath.'/'.$package);
	    		echo 'WARNING '.$libspath.'/'.$package.' DOESNT EXISTS: you must add frontawesome scss files<br>';
	    	}

	    	//DELETE TO UPDATE
	    	if(JFolder::exists($libspath.'/'.$package.'/fontawesome5')){
	    		JFolder::delete($libspath.'/'.$package.'/fontawesome5');
	    	}
    		JFolder::create($libspath.'/'.$package.'/fontawesome5');
	    	echo 'create '.$libspath.'/'.$package.'/fontawesome5<br>';


	    	if(!JFolder::exists($libspath.'/'.$package.'/fontawesome5pro')){
	    		JFolder::create($libspath.'/'.$package.'/fontawesome5pro');
	    		echo 'create '.$libspath.'/'.$package.'/fontawesome5pro<br>';
	    	}


	    	//COPY FILES

	    	//webfonts
	    	JFolder::copy($vendorpath . '/fortawesome/font-awesome/webfonts', $libspath.'/'.$package.'/fontawesome5/webfonts');
	    	$svgList = glob($libspath.'/'.$package.'/fontawesome5/webfonts/*.svg');   	
	    	//delete svg
	    	JFile::delete($svgList);

	    	//SCSS
	    	JFolder::copy($vendorpath . '/fortawesome/font-awesome/scss', $libspath.'/'.$package.'/fontawesome5/scss');

	    	//js
	    	JFolder::copy($vendorpath . '/fortawesome/font-awesome/js', $libspath.'/'.$package.'/fontawesome5/js');
	    	JFile::copy($vendorpath . '/fortawesome/font-awesome/LICENSE.txt', $libspath.'/'.$package.'/fontawesome5/LICENSE.txt');

	    	
	    	echo 'create '.$package.'<br>';

	    break;
	    case 'scssphp':


	    	//DELETE TO UPDATE
	    	if(JFolder::exists($libspath.'/'.$package)){
	    		JFolder::delete($libspath.'/'.$package);
	    	}
    		JFolder::create($libspath.'/'.$package);

	    	
	    	//COPY FILES
    		JFolder::copy($vendorpath . '/scssphp/scssphp/src', $libspath.'/'.$package.'/src');
    		JFile::copy($vendorpath . '/scssphp/scssphp/scss.inc.php', $libspath.'/'.$package.'/scss.inc.php');
    		JFile::copy($vendorpath . '/scssphp/scssphp/LICENSE.md', $libspath.'/'.$package.'/LICENSE.md');

	        echo 'create '.$package.'<br>';

	    break;
	    case 'bootstrap':

	    	//DELETE TO UPDATE
	    	if(JFolder::exists($libspath.'/'.$package)){
	    		JFolder::delete($libspath.'/'.$package);
	    	}
	    	JFolder::create($libspath.'/'.$package);


	    	//COPY FILES
    		JFolder::copy($vendorpath . '/twbs/bootstrap/scss', $libspath.'/'.$package.'/scss');
    		JFolder::copy($vendorpath . '/twbs/bootstrap/js', $libspath.'/'.$package.'/js');
    		JFolder::delete($libspath.'/'.$package.'/js/tests');
    		JFolder::delete($libspath.'/'.$package.'/js/src');
    		$mapList = glob($libspath.'/'.$package.'/js/dist/*.map');
    		JFile::delete($mapList);

    		JFolder::copy($vendorpath . '/twbs/bootstrap/dist', $libspath.'/'.$package.'/dist');
    		$mapList = glob($libspath.'/'.$package.'/dist/css/*.map');
    		JFile::delete($mapList);
    		$mapList = glob($libspath.'/'.$package.'/dist/js/*.map');
    		JFile::delete($mapList);

    		echo 'create '.$package.'<br>';


	    break;

	    case 'mobiledetectlib':


	    	//DELETE TO UPDATE
	    	if(JFolder::exists($libspath.'/'.$package)){
	    		JFolder::delete($libspath.'/'.$package);
	    	}
	    	JFolder::create($libspath.'/'.$package);

	    	JFile::copy($vendorpath . '/mobiledetect/mobiledetectlib/Mobile_Detect.php', $libspath.'/'.$package.'/Mobile_Detect.php');
	    	JFile::copy($vendorpath . '/mobiledetect/mobiledetectlib/LICENSE.txt', $libspath.'/'.$package.'/LICENSE.txt');




    		echo 'create '.$package.'<br>';


	    break;
	}


}

